from flask import Flask, request, jsonify, render_template
from odm.the_string import My_data
from db.connect import Connect_service


app = Flask(__name__)

# @app.route('/', methods=['GET'])
# def home():
#     return jsonify(
#         message="Alive"
#     )


@app.route('/', methods=['GET', 'POST'])
def home():
    if request.method == 'GET':
        data_string_id = request.args.get('data_string_id')
        connect_db = Connect_service()
        my_data_document_results = My_data.objects(
            data_string_id=data_string_id)
        if my_data_document_results:
            for item in my_data_document_results:
                string_value = item.data_string
                connect_db.db.close()
        else:
            string_value = 'These are not the droids you are looking for..'
        return render_template(
            'index.html',
            string_value_html=string_value
        ), 200

    elif request.method == 'POST':
        if request.is_json:
            string_data = request.json['data_string']
            string_data_id = request.json['data_string_id']

        elif request.args:
            string_data = request.args.get('data_string')
            string_data_id = request.args.get('data_string_id')
        else:
            string_data = request.form['data_string']
            string_data_id = request.form['data_string_id']

        connect_db = Connect_service()
        my_data_document = My_data(
            data_string=string_data,
            data_string_id=string_data_id
        )
        my_data_document.save()
        connect_db.db.close()

        return jsonify(
            Status="Successfully added string"
        ), 200


if __name__ == '__main__':
    app.run(debug=True)
