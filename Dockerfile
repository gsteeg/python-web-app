FROM python:3.7-slim-buster

RUN mkdir /app
WORKDIR /app
ADD . .
RUN ls
ADD requirements.txt .
RUN pip install -r requirements.txt
EXPOSE 3000

CMD ["gunicorn", "app:app", "-b", "0.0.0.0:3000", "--workers", "4"]