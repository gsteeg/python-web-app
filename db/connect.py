from mongoengine import connect, disconnect
from pymongo.errors import ServerSelectionTimeoutError
import os


class Connect_service:
    def __init__(self):
        try:
            self.db=connect(
                db=os.environ['MONGO_DB_NAME'],
                host=os.environ['MONGO_DB_HOST'],
                port=27017,
            )
        except ServerSelectionTimeoutError as connection_error:
            self.connect_error = {
                "error_status":"connection error",
                "error_detail":str(connection_error)
            }
